<?php namespace RW\Utils\Classes\Traits;

//use RW\Utils\Classes\ActivatableScope;
use App;
use Event;
use RW\Utils\Models\DynamicSeo;
use Yaml;

trait SeoExtendable
{
    public static function bootSeoExtendable()
    {
        self::extend(function ($model) {
            $model->morphOne['rwSeo'] = [
                DynamicSeo::class,
                'name' => 'seoable'
            ];
        });

        Event::listen('backend.form.extendFields', function (\Backend\Widgets\Form $formWidget) {
            if ($formWidget->model instanceof \RW\Utils\Models\DynamicSeo) {
                return;
            }
            $isRelation = request('_relation_field');
            if (!is_null($isRelation) && ($isRelation != 'rwSeo')) {
                return;
            }
            try {
                $formWidget->addSecondaryTabFields([
                   'rwSeo' => [
                       'type' => 'partial',
                       'path' => '$/rw/utils/partials/_rwSeo.htm',
                       'span' => 'full',
                       'context' => 'update',
                       'tab' => 'Seo'
                    ]
                ]);
            } catch (\Exception $e) {
                \Log::error($e->getMessage());
            }
            return true;
        });
    }
}
